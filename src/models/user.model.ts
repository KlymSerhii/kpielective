export class User {
  id: string;
  _id?: string; // just for data from BE in electives
  login: string;
  password: string;
  name?: string;
  surname?: string;
  email?: string;
  dateOfBirth?: Date;
  role?: string;
  group?: string;

  constructor() {
    this.id = '';
    this.login ='';
    this.password ='';
    this.name ='';
    this.surname = '';
    this.email = '';
    this.dateOfBirth = new Date();
    this.role ='';
    this.group = '';
  }
}
