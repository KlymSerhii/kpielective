import {User} from "./user.model";

export class Notification {
  electiveId: string;
  text: string;
  users: User[];
}
