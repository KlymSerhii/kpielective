import {User} from "./user.model";
import {Audience} from "./audience.model";

export class ElectiveModel {
  id: string;
  name: string;
  author?: User;
  date: Date;
  time: Date;
  duration: Date;
  audience: Audience;
  description: string;
  privateElective: string;
  subscribers: User[];

  constructor() {
    this.id = '';
    this.name = '';
    this.author = new User();
    this.date = new Date();
    this.time = new Date();
    this.audience = new Audience();
    this.description = '';
    this.subscribers = [];
    this.privateElective = 'Open for everyone'
  }
}
