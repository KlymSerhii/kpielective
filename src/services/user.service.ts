import {Injectable} from "@angular/core";
import {Storage} from "@ionic/storage";
import {User} from "../models/user.model";
import {Headers, Http, Response} from "@angular/http";
import 'rxjs/Rx';
import {Observable} from "rxjs/Observable";

@Injectable()
export class UserService {
  private currentUser: User;
  private allUsers: User[] = [];

  constructor(private storage: Storage, private http: Http) {
  }

  logOut(): Promise<any> {
    this.currentUser = undefined;
    return this.storage.remove('user');
  }

  setActiveUser(user: User) {
    this.currentUser = user;
    this.storage.set('user', this.currentUser);
  }

  getActiveUser() { // getting from Storage to login
    return this.storage.get('user')
      .then(
        (user) => {
          this.currentUser = user == null ? undefined : user;
          return this.currentUser;
        }
      );
  }

  getUser(): User { // getting from service
    return this.currentUser;
  }


  checkLogInUser(user: User): User {
    let allUsers = this.allUsers;
    for (let i = 0; i < allUsers.length; i++) {

      if (user.login === allUsers[i].login && user.password === allUsers[i].password) {
        user.id = allUsers[i].id;
        user.dateOfBirth = allUsers[i].dateOfBirth;
        user.name = allUsers[i].name;
        user.surname = allUsers[i].surname;
        user.role = allUsers[i].role;
        user.email = allUsers[i].email;
        user.group = allUsers[i].group;
        this.setActiveUser(user);
        return user;

      }
    }

    return undefined;
  }

  getAllUsersFromService(): User[] {
    return this.allUsers;
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get('https://kpi-elective.herokuapp.com/getAllUsers')
      .map((response: Response) => {
        const users = response.json();
        let transformedUsers: User[] = [];
        for (let user of users) {
          let newUser = new User();
          newUser.id = user._id;
          newUser.name = user.name;
          newUser.email = user.email;
          newUser.surname = user.surname;
          newUser.dateOfBirth = user.dateOfBirth;
          newUser.group = user.group;
          newUser.role = user.role;
          newUser.password = user.password;
          newUser.login = user.login;
          transformedUsers.push(newUser);
        }
        this.allUsers = transformedUsers;
        return transformedUsers;
      })

  }

  addUser(user: User) {
    const body = JSON.stringify(user);
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post("https://kpi-elective.herokuapp.com/addUser", body, {headers: headers})
      .map((response: Response) => {
        const result = response.json();
        let user: User = new User();
        user.id = result._id;
        user.name = result.name;
        user.email = result.email;
        user.surname = result.surname;
        user.dateOfBirth = result.dateOfBirth;
        user.group = result.group;
        user.role = result.role;
        user.password = result.password;
        user.login = result.login;
        this.allUsers.push(user);
        return user;
      })
  }
}
