import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Response, Http, Headers} from "@angular/http";
import {Group} from "../models/group.model";

import 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import {Audience} from "../models/audience.model";
import {Comment} from "../models/comment.model";
import {Notification} from "../models/notification.model";

@Injectable()
export class DataService {
  groups: Group[];
  audiences: Audience[];

  constructor(private http: Http) {
  }

  getAllGroups(): Observable<any> {
    return this.http.get('https://kpi-elective.herokuapp.com/getAllGroups')
      .map((response: Response) => {
        const groups = response.json();
        let transformedGroups: Group[] = [];
        for (let group of groups) {
          let newGroup = new Group();
          newGroup.id = group._id;
          newGroup.name = group.name;
          transformedGroups.push(newGroup);
        }
        this.groups = transformedGroups;
        return transformedGroups;
      });
  }

  getAllAudiences(): Observable<any> {
    return this.http.get('https://kpi-elective.herokuapp.com/getAllAudiences')
      .map((response: Response) => {
        const audiences = response.json();
        let transformedAudiences: Audience[] = [];
        for (let audience of audiences) {
          let newAudience = new Audience();
          newAudience.id = audience._id;
          newAudience.name = audience.name;
          transformedAudiences.push(newAudience);
        }
        this.audiences = transformedAudiences;
        return transformedAudiences;
      });
  }

  addGroup(group: string) {
    const body = JSON.stringify(group);
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post("https://kpi-elective.herokuapp.com/addGroup", body, {headers: headers})
      .map((response: Response) => {
        const group = response.json();
        let newGroup: Group = new Group();
        newGroup.id = group._id;
        newGroup.name = group.name;

        this.groups.push(newGroup);
        return newGroup;
      })
  }

  addAudience(audience: string) {
    const body = JSON.stringify(audience);
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post("https://kpi-elective.herokuapp.com/addAudience", body, {headers: headers})
      .map((response: Response) => {
        const audience = response.json();
        let newAudience: Audience = new Audience();
        newAudience.id = audience._id;
        newAudience.name = audience.name;

        this.audiences.push(newAudience);
        return newAudience;
      })
  }

  deleteGroup(group: Group) {
    this.groups.splice(this.groups.indexOf(group), 1);
    return this.http.delete("https://kpi-elective.herokuapp.com/deleteGroup/" + group.id)
      .map((response: Response) => response.json())
  }

  deleteAudience(audience: Audience) {
    this.audiences.splice(this.audiences.indexOf(audience), 1);
    return this.http.delete("https://kpi-elective.herokuapp.com/deleteAudience/" + audience.id)
      .map((response: Response) => response.json())
  }

  sendComment(comment: Comment) {

  }

  sendNotification(notification: Notification) {

  }

}
