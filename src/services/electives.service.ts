import {Injectable} from "@angular/core";
import {ElectiveModel} from "../models/elective.model";
import {User} from "../models/user.model";
import {Observable} from "rxjs/Observable";
import {Headers, Http, Response} from "@angular/http";

@Injectable()
export class ElectivesService {
  private electives: ElectiveModel[] = [];

  constructor(private http: Http) {
  }


  addElective(elective: ElectiveModel) {
    const body = JSON.stringify(elective);
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post("https://kpi-elective.herokuapp.com/addElective", body, {headers: headers})
      .map((response: Response) => {
        const elective = response.json();
        let newElective: ElectiveModel = new ElectiveModel();
        newElective.id = elective._id;
        newElective.name = elective.name;
        newElective.author = elective.author;
        newElective.date = elective.date;
        newElective.time = elective.time;
        newElective.audience = elective.audience;
        newElective.duration = elective.duration;
        newElective.description = elective.description;
        newElective.privateElective = elective.privateElective;
        newElective.subscribers = elective.subscribers;

        this.electives.push(newElective);
        return newElective;
      })
  }

  editElective(elective: ElectiveModel) {
    const body = JSON.stringify(elective);
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.patch("https://kpi-elective.herokuapp.com/patchElective/" + elective.id, body, {headers: headers})
      .map((response: Response) => response.json())
  }

  deleteElective(elective: ElectiveModel) {
    this.electives.splice(this.electives.indexOf(elective), 1);
    return this.http.delete("https://kpi-elective.herokuapp.com/deleteElective/" + elective.id)
      .map((response: Response) => response.json())
  }

  getElectives() {
    return this.http.get('https://kpi-elective.herokuapp.com/getAllElectives')
      .map((response: Response) => {
        const electives = response.json();
        let transformedElectives: ElectiveModel[] = [];
        for (let elective of electives) {
          let newElective = new ElectiveModel();
          newElective.id = elective._id;
          newElective.name = elective.name;
          newElective.author = elective.author;
          newElective.date = elective.date.slice(0, 10);
          newElective.time = elective.time;
          newElective.audience = elective.audience;
          newElective.duration = elective.duration;
          newElective.description = elective.description;
          newElective.privateElective = elective.privateElective;
          newElective.subscribers = elective.subscribers;
          transformedElectives.push(newElective);
        }
        this.electives = transformedElectives;
        return transformedElectives;
      })

  }

  addSubscriber(elective: ElectiveModel, user: User): Observable<any> {
    let index = this.electives.indexOf(elective);
    this.electives[index].subscribers.push(user);

    const body = JSON.stringify({electiveId: elective.id, userId: user.id});
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post("https://kpi-elective.herokuapp.com/addSubscriberToElective", body, {headers: headers})

  }

  deleteSubscriber(elective: ElectiveModel, user: User): Observable<any> {
    let indexOfElective = this.electives.indexOf(elective);
    let indexOfSubscriber = this.electives[indexOfElective].subscribers.indexOf(user);
    this.electives[indexOfElective].subscribers.splice(indexOfSubscriber, 1);

    const body = JSON.stringify({electiveId: elective.id, userId: user.id});
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post("https://kpi-elective.herokuapp.com/deleteSubscriberToElective", body, {headers: headers})
  }
}
