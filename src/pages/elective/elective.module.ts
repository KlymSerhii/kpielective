import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Elective } from './elective';
import {Ionic2RatingModule} from "ionic2-rating";


@NgModule({
  declarations: [
    Elective,
  ],
  imports: [
    IonicPageModule.forChild(Elective),
    Ionic2RatingModule
  ],
  exports: [
    Elective
  ]
})
export class ElectiveModule {}
