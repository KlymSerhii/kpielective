import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {ElectivesService} from "../../services/electives.service";
import {UserService} from "../../services/user.service";
import {ElectiveModel} from "../../models/elective.model";
import {Validators, FormBuilder, FormGroup} from "@angular/forms";
import {User} from "../../models/user.model";
import {Audience} from "../../models/audience.model";
import {DataService} from "../../services/data.service";
import {Comment} from "../../models/comment.model";
import {Notification} from "../../models/notification.model";

@IonicPage()
@Component({
  selector: 'page-elective',
  templateUrl: 'elective.html',
})
export class Elective {
  group: FormGroup;
  comment: FormGroup;
  notification: FormGroup;

  elective: ElectiveModel;
  status: string;

  newElective: ElectiveModel = new ElectiveModel();
  subscribers: User[] = [];

  allUsers: User[] = [];

  allAudiences: Audience[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private electivesService: ElectivesService, private userService: UserService,
              private viewCtrl: ViewController, private formBuilder: FormBuilder, private dataService: DataService) {

    this.status = navParams.get('status');
    this.elective = navParams.get('elective');

    let month: string = (new Date().getMonth() + 1) + '';
    if (month.length === 1) {
      month = '0' + month;
    }
    let day: string = new Date().getDate() + '';
    if (day.length === 1) {
      day = '0' + day;
    }


    let currDate: string = new Date().getFullYear() + '-' + month + '-' + day;

    this.group = this.formBuilder.group({
      name: ['', Validators.required],
      date: [currDate, Validators.required],
      time: ['', Validators.required],
      duration: ['', Validators.required],
      audience: [''],
      description: ['', Validators.required],
      privateElective: [false],
      subscribers: [[]]
    });


    this.comment = this.formBuilder.group({
      rate: [0,Validators.required],
      text: ['', Validators.required]
    });

    this.notification = this.formBuilder.group({
      text: ['', Validators.required]
    });



  }

  ionViewDidLoad() {

    this.getAllUsers();
    if (this.status === 'edit' || this.status === 'tap') {
      let privateElective = false;
      if (this.elective.privateElective === 'private') {
        privateElective = true;
      }
      let subscribers = this.elective.subscribers;
      let newSubscribers: string[] = [];
      for (let subscriber of subscribers) {
        newSubscribers.push(subscriber._id);
      }
      this.group = this.formBuilder.group({
        name: [this.elective.name, Validators.required],
        date: [this.elective.date, Validators.required],
        time: [this.elective.time, Validators.required],
        duration: [this.elective.duration, Validators.required],
        audience: [this.elective.audience, Validators.required],
        description: [this.elective.description, Validators.required],
        privateElective: [privateElective],
        subscribers: [newSubscribers]
      });

    }

    this.dataService.getAllAudiences().subscribe((audiences => this.allAudiences = audiences));
  }


  onAddElective(): void {
    if (this.status === 'edit') {

      this.newElective.id = this.elective.id;

    }

    this.newElective.author = this.userService.getUser();
    this.newElective.date = this.group.value.date;
    this.newElective.time = this.group.value.time;
    this.newElective.duration = this.group.value.duration;
    this.newElective.name = this.group.value.name;
    this.newElective.audience = this.group.value.audience;
    this.newElective.description = this.group.value.description;
    this.newElective.subscribers = this.group.value.subscribers;

    if (this.group.value.privateElective === true) {

      this.newElective.privateElective = 'private'

    } else {

      this.newElective.privateElective = 'public'

    }

    if (this.status === 'edit') {

      this.electivesService.editElective(this.newElective).subscribe();

    }

    if (this.status === 'add') {

      this.electivesService.addElective(this.newElective).subscribe();

    }


    this.closeModal();
  }

  onAddComment(): void {
    let comment: Comment = new Comment();
    comment.rate = this.comment.value.rate;
    comment.text = this.comment.value.text;
    comment.electiveId = this.elective.id;
    this.dataService.sendComment(comment);
    this.closeModal();
  }

  onAddNotification(): void {
    let notification: Notification = new Notification();
    notification.text = this.notification.value.text;
    notification.users = this.group.value.subscribers;
    notification.electiveId = this.elective.id;
    this.dataService.sendNotification(notification);
    this.closeModal();
  }

  getAllUsers(): void {
    this.userService.getAllUsers()
      .subscribe(
        (users: User[]) => {
          this.allUsers = users;
        });
  }

  getCurrDate(): Date {
    return new Date();
  }

  closeModal(): void {
    this.viewCtrl.dismiss();
  }

  getCurrentUser(): User {
    return this.userService.getUser();
  }

}
