import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../models/user.model";
import {UserService} from "../../services/user.service";
import {DataService} from "../../services/data.service";
import {Group} from "../../models/group.model";
@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class Registration {
  registration: FormGroup;

  user: User;

  groups: Group[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController,
              private formBuilder: FormBuilder, private userService: UserService, private dataService: DataService) {

    this.registration = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required],
      name: ['', Validators.required],
      surname: ['', Validators.required],
      email: ['', Validators.required],
      dateOfBirth: ['', Validators.required],
      role: ['', Validators.required],
      group: ['', Validators.required],

    });

  }

  ionViewWillEnter() {
    this.dataService.getAllGroups().subscribe((groups) => {
      this.groups = groups;
    });
  }


  closeModal(user?: User): void {
    if( user ) {

      this.viewCtrl.dismiss(user);

    } else {

      this.viewCtrl.dismiss();

    }
  }

  onAddUser(): void {
    this.user = new User();
    this.user.login = this.registration.value.login;
    this.user.password = this.registration.value.password;
    this.user.name = this.registration.value.name;
    this.user.surname = this.registration.value.surname;
    this.user.email = this.registration.value.email;
    this.user.dateOfBirth = this.registration.value.dateOfBirth;
    this.user.role = this.registration.value.role;
    this.user.group = this.registration.value.group;

    this.userService.addUser(this.user)
      .subscribe(
        data => this.closeModal(this.user),
        error => console.log(error),
      );
  }

}
