import {Component} from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams, Refresher} from 'ionic-angular';
import {UserService} from "../../services/user.service";
import {DataService} from "../../services/data.service";
import {Group} from "../../models/group.model";
import {Audience} from "../../models/audience.model";

@IonicPage()
@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})
export class Admin {
  groups: Group[];
  audiences: Audience[];

  addFilter = 'groups';

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private userService: UserService, private alertCtrl: AlertController,
              private dataService: DataService) {
  }

  ionViewWillEnter() {
    this.refreshData();
  }

  logOut() {
    this.userService.logOut().then(() => this.navCtrl.popToRoot());
  }


  loadPrompt(): void {
    if (this.addFilter === 'groups') {

      let alert = this.alertCtrl.create({
        title: 'Введіть назву групи',
        inputs: [
          {
            name: 'name'
          }
        ],
        buttons: [
          {
            text: 'Відмінити',
            role: 'cancel',
            handler: data => {
            }
          },
          {
            text: 'Зберегти',
            handler: data => {
              this.dataService.addGroup(data).subscribe();
              this.refreshData();
            }
          }
        ]
      });
      alert.present();

    }

    if (this.addFilter === 'audiences') {

      let alert = this.alertCtrl.create({
        title: 'Введіть номер аудиторії',
        inputs: [
          {
            name: 'name'
          }
        ],
        buttons: [
          {
            text: 'Відмінити',
            role: 'cancel',
            handler: data => {
            }
          },
          {
            text: 'Зберегти',
            handler: data => {
              this.dataService.addAudience(data).subscribe();
              this.refreshData();
            }
          }
        ]
      });
      alert.present();

    }


  }

  deleteGroup(group: Group) {
    this.dataService.deleteGroup(group).subscribe();
  }

  deleteAudience(audience: Audience) {
    this.dataService.deleteAudience(audience).subscribe();
  }

  refreshData(refresher?: Refresher): void {

    if (this.addFilter === 'groups') {
      this.dataService.getAllGroups().subscribe((groups) => {
        this.groups = groups;

        if (refresher) {
          refresher.complete();
        }
      });
    }

    if (this.addFilter === 'audiences') {
      this.dataService.getAllAudiences().subscribe((audiences) => {
        this.audiences = audiences;

        if (refresher) {
          refresher.complete();
        }
      });
    }

    if (this.addFilter === 'teachers') {

        if (refresher) {
          refresher.complete();
        }
    }
  }

}
