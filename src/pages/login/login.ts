import {Component} from '@angular/core';
import {IonicPage, LoadingController, ModalController, NavController, NavParams, ToastController} from 'ionic-angular';
import {User} from "../../models/user.model";
import {UserService} from "../../services/user.service";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
  user: User;

  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController,
              private loadingCtrl: LoadingController, private userService: UserService,
              private  modalCtrl: ModalController) {
  }

  ionViewWillLoad() {
    this.firstSignInLoading();
  }

  logIn(user: User): void {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present().then(() => {
        this.userService.getAllUsers().subscribe(() => {
          this.user = this.userService.checkLogInUser(user);
          if (this.user) {
            let role = this.user.role;
            if (role === 'Студент') {

              this.navCtrl.push('Student');

            }

            if (role === 'Викладач') {

              this.navCtrl.push('Teacher');

            }

            if (role === 'admin') {

              this.navCtrl.push('Admin');

            }

          }
          loader.dismiss();

          if (!this.user) {

            let toast = this.toastCtrl.create({
              message: 'You have an error. Please try again.',
              duration: 3000,
              position: 'top'
            });
            toast.present();
          }
        });

      }
    );


  }

  firstSignInLoading(): void {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present().then(() => {
        this.userService.getActiveUser().then(user => {
          this.user = user;
          this.userService.getAllUsers().subscribe();
          if (this.user) {
            let role = this.user.role;

            if (role === 'Студент') {

              this.navCtrl.push('Student');

            }

            if (role === 'Викладач') {

              this.navCtrl.push('Teacher');

            }

            if (role === 'admin') {

              this.navCtrl.push('Admin');

            }
          }

          loader.dismiss();
        });
      }
    );
  }

  loadRegistrationPage(): void {
    let registerModal = this.modalCtrl.create('Registration');

    registerModal.onDidDismiss((user: User) => {

      if (user) {
        this.logIn(user);
      }

    });

    registerModal.present().then();

  }

}
