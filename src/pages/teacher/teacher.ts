import {Component} from '@angular/core';
import {ActionSheetController, IonicPage, ModalController, NavController, NavParams, Refresher} from 'ionic-angular';
import {ElectivesService} from "../../services/electives.service";
import {UserService} from "../../services/user.service";
import {ElectiveModel} from "../../models/elective.model";

import * as _ from 'lodash';
import {User} from "../../models/user.model";


@IonicPage()
@Component({
  selector: 'page-teacher',
  templateUrl: 'teacher.html',
})
export class Teacher {

  electives: ElectiveModel[] = [];
  allElectives: ElectiveModel[] = [];

  electiveFilter: string = 'all';

  mark: number=0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private userService: UserService,
              private electivesService: ElectivesService, public modalCtrl: ModalController,
              private actionSheetCtrl: ActionSheetController) {
  }

  ionViewDidLoad() {
    this.mark = this.getRand();
  }

  ionViewWillEnter() {
    this.refreshData();
  }

  refreshData(refresher?: Refresher): void {
    this.electivesService.getElectives().subscribe((electives) => {
      electives.sort((a, b) => {
        let aDate = new Date(a.date + 'T' + a.time).getTime(), bDate = new Date(b.date + 'T' + b.time).getTime();
        if (aDate !== bDate) {

          if (aDate > bDate) {
            return 1;
          }
          if (aDate < bDate) {
            return -1;
          }

        }
        return 0;
      });

      this.allElectives = electives;
      this.filterElectives();

      if (refresher) {
        refresher.complete();
      }
    });
  }

  presentActionSheet(elective: ElectiveModel, event: Event) {
    event.stopPropagation();

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Редагувати факультатив',
      buttons: [
        {
          text: 'Видалити',
          role: 'destructive',
          handler: () => {

            this.electivesService.deleteElective(elective).subscribe(result => {
              console.log(result);
              this.refreshData();
            });

          }
        },
        {
          text: 'Редагувати',
          handler: () => {
            this.loadElectivePage('edit', elective);
          }
        },
        {
          text: 'Відмінити',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });

    actionSheet.present();
    return false;
  }


  loadElectivePage(status?: string, elective?: ElectiveModel, event?: Event): void {
    let electiveModal = this.modalCtrl.create('Elective', {status: status});

    if (elective) {

      electiveModal = this.modalCtrl.create('Elective', {elective: elective, status: status});

    }

    electiveModal.onDidDismiss(() => {
      this.refreshData();
      this.filterElectives();
    });

    electiveModal.present().then();
  }

  logOut() {
    this.userService.logOut().then(() => this.navCtrl.popToRoot());
  }

  filterElectives() {
    let user = this.getUser();

    if (this.electiveFilter === 'all') {

      this.electives = _.filter(this.allElectives, elective => ((elective.privateElective !== 'private')
      || (elective.author === user._id) || (elective.subscribers.indexOf(user) !== -1) )
      && this.checkIfNewElective(elective));

    }

    if (this.electiveFilter === 'my') {

      this.electives = _.filter(this.allElectives, elective => elective.author === user.id && this.checkIfNewElective(elective));

    }

    if (this.electiveFilter === 'old') {
      this.electives = _.filter(this.allElectives, elective => ((elective.privateElective !== 'private')
      || (elective.author === user._id) || (elective.subscribers.indexOf(user) !== -1) )
      && !this.checkIfNewElective(elective));
    }
  }

  checkIfNewElective(elective): boolean {
    return new Date(elective.date + 'T' + elective.time).getTime() > new Date().getTime();
  }

  getUser(): User {
    return this.userService.getUser();
  }

  swipeEvent(event) {
    if (event.direction === 2 && this.electiveFilter === 'all') {
      this.electiveFilter = 'my';
    }

    if (event.direction === 4 && this.electiveFilter === 'my') {
      this.electiveFilter = 'all';
    }

    this.filterElectives();

  }

  getAuthorName(id: string): string {
    for (let user of this.userService.getAllUsersFromService()) {
      if (user.id === id) {
        return user.name + ' ' + user.surname;
      }
    }
    return '';
  }

  getRand(): number {
    return Math.floor(Math.random()*5) + 1;
  }

}
