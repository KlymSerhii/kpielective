import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, Refresher} from 'ionic-angular';
import {UserService} from "../../services/user.service";
import {User} from "../../models/user.model";
import {ElectiveModel} from "../../models/elective.model";
import * as _ from 'lodash';
import {ElectivesService} from "../../services/electives.service";


@IonicPage()
@Component({
  selector: 'page-student',
  templateUrl: 'student.html',
})
export class Student {
  user: User;
  electiveFilter: string = 'all';

  electives: ElectiveModel[] = [];
  allElectives: ElectiveModel[] = [];

  mark: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private userService: UserService,
              private electivesService: ElectivesService, private modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    this.mark = this.getRand();
  }

  ionViewWillEnter() {
    this.refreshData();
    this.user = this.getUser();
  }

  refreshData(refresher?: Refresher): void {
    this.electivesService.getElectives().subscribe((electives) => {
      electives.sort((a, b) => {
        let aDate = new Date(a.date + 'T' + a.time).getTime(), bDate = new Date(b.date + 'T' + b.time).getTime();
        if (aDate !== bDate) {

          if (aDate > bDate) {
            return 1;
          }
          if (aDate < bDate) {
            return -1;
          }

        }
        return 0;
      });

      this.allElectives = electives;
      this.filterElectives();

      if (refresher) {
        refresher.complete();
      }
    });
  }

  filterElectives() {
    let user = this.user;

    if (this.electiveFilter === 'all') {

      this.electives = _.filter(this.allElectives, elective => ((elective.privateElective !== 'private')
      || (this.checkUserSubscription(elective, user))) && this.checkIfNewElective(elective));

    }

    if (this.electiveFilter === 'my') {

      this.electives = _.filter(this.allElectives, elective => this.checkUserSubscription(elective, user)
      && this.checkIfNewElective(elective));

    }

    if (this.electiveFilter === 'old') {

      this.electives = _.filter(this.allElectives, elective => ((elective.privateElective !== 'private')
      || (this.checkUserSubscription(elective, user))) && !this.checkIfNewElective(elective));

    }
  }


  checkIfNewElective(elective): boolean {
    return new Date(elective.date + 'T' + elective.time).getTime() > new Date().getTime();
  }

  swipeEvent(event) {
    if (event.direction === 2 && this.electiveFilter === 'all') {
      this.electiveFilter = 'my';
    }

    if (event.direction === 4 && this.electiveFilter === 'my') {
      this.electiveFilter = 'all';
    }

    this.filterElectives();

  }

  loadElectivePage(status: string, elective: ElectiveModel): void {
    let electiveModal = this.modalCtrl.create('Elective', {elective: elective, status: status});

    electiveModal.onDidDismiss(() => {
      this.refreshData();
      this.filterElectives();
    });

    electiveModal.present().then();
  }

  subscribe(elective: ElectiveModel, event: Event): void {
    event.stopPropagation();
    let user = this.user;
    this.electivesService.addSubscriber(elective, user).subscribe(() => this.refreshData());
  }

  unsubscribe(elective: ElectiveModel, event: Event): void {
    event.stopPropagation();
    let user = this.user;
    this.electivesService.deleteSubscriber(elective, user).subscribe(() => this.refreshData());
  }

  getUser(): User {
    return this.userService.getUser();
  }

  logOut() {
    this.userService.logOut().then(() => this.navCtrl.popToRoot());
  }

  checkUserSubscription(elective: ElectiveModel, user: User) {

    for (let subscriber of elective.subscribers) {
      if (subscriber._id === user.id) {
        return true;
      }
    }

    return false;
  }

  getAuthorName(id: string): string {
    for (let user of this.userService.getAllUsersFromService()) {
      if (user.id === id) {
        return user.name + ' ' + user.surname;
      }
    }
    return '';
  }

  getRand(): number {
    return Math.floor(Math.random()*5) + 1;
  }

}
