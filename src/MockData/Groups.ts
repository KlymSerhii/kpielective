export const GROUPS: {name: string}[] = [
  {
    name: 'Викладачі'
  },
  {
    name: 'ІС-31'
  },
  {
    name: 'ІС-32'
  },
  {
    name: 'ІС-33'
  },
  {
    name: 'ІС-41'
  },
  {
    name: 'ІС-42'
  },
  {
    name: 'ІС-43'
  },
  {
    name: 'ІС-51'
  },
  {
    name: 'ІС-52'
  },
  {
    name: 'ІС-53'
  },
  {
    name: 'ІС-61'
  },
  {
    name: 'ІС-62'
  },
  {
    name: 'ІС-63'
  }
];
